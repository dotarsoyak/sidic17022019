<script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>

<ol class="breadcrumb">
  <li><a href="<?php echo Yii::app()->createUrl('budget/list', array('id'=>$projectid)); ?>">Presupuestos</a></li>
  <li class="active">
    <?php echo "$model->id - $model->name"; ?>
  </li>
</ol>

<?php if(Yii::app()->user->hasFlash("updated")): ?>
<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
  <?php echo Yii::app()->user->getFlash("updated"); ?>
</div>
<?php endif; ?>

<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
    <small class="text-muted">Los campos con asterisco son requeridos</small>
    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'BudgetForm',
        'enableAjaxValidation'=>false,
    )); ?>

    <?php CHtml::$errorContainerTag = 'div'; ?>

    <div class="row">
      <label for="BudgetForm_name">*Nombre</label>
      <input value="<?php echo $model->name; ?>" name="BudgetForm[name]" 
             type="text" id="BudgetForm_name" class="form-control" placeholder=""  />
      <?php echo $form->error($model,'name', array('class'=>'error')); ?>
    </div>

    <div class="row">
      <br/>
      <button type="submit" class="btn btn-raised btn-primary m-t-15 waves-effect">Guardar</button>
      <?php echo CHtml::link('Regresar', Yii::app()->createUrl('budget/list', array('id'=>$projectid)) ,array('class'=>'btn btn-raised btn-default m-t-15 waves-effect')); ?>
    </div>

    <?php $this->endWidget(); ?>
    </div>
</div>


<style>
  div.error{color:#e61616;}
</style>