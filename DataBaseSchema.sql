-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.21 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando estructura para tabla sidic.authassignment
CREATE TABLE IF NOT EXISTS `authassignment` (
  `itemname` varchar(64) NOT NULL,
  `userid` varchar(64) NOT NULL,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`itemname`,`userid`),
  CONSTRAINT `AuthAssignment_ibfk_1` FOREIGN KEY (`itemname`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.authitem
CREATE TABLE IF NOT EXISTS `authitem` (
  `name` varchar(64) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `bizrule` text,
  `data` text,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.authitemchild
CREATE TABLE IF NOT EXISTS `authitemchild` (
  `parent` varchar(64) NOT NULL,
  `child` varchar(64) NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`),
  CONSTRAINT `AuthItemChild_ibfk_1` FOREIGN KEY (`parent`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `AuthItemChild_ibfk_2` FOREIGN KEY (`child`) REFERENCES `authitem` (`name`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.budget
CREATE TABLE IF NOT EXISTS `budget` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectid` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'Solo hay dos status 1 - activo; 0-Inactivo',
  PRIMARY KEY (`id`),
  KEY `FK_budget_project` (`projectid`),
  CONSTRAINT `FK_budget_project` FOREIGN KEY (`projectid`) REFERENCES `project` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='tabla para guardar los presupuestos';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.budgetitem
CREATE TABLE IF NOT EXISTS `budgetitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `budgetid` int(11) NOT NULL,
  `fatherid` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `budgettop` bigint(20) NOT NULL COMMENT 'es un tope que se establece para la compra de insumos por partida, este tope no debe rebasarse, si el tope es cero quiere decir que no tiene tope',
  `sort` smallint(6) NOT NULL DEFAULT '0',
  `status` smallint(6) NOT NULL DEFAULT '1' COMMENT 'la partida puede estar 1:activa; 0:inactiva',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `FK_budgetItem_budget` (`budgetid`),
  CONSTRAINT `FK_budgetItem_budget` FOREIGN KEY (`budgetid`) REFERENCES `budget` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8 COMMENT='guarda la relacion entre pregupuesto y partida - subpartida\r\n';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.fiscal
CREATE TABLE IF NOT EXISTS `fiscal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'calle y numero',
  `country` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `rfc` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_fiscaldata_user` (`userid`),
  CONSTRAINT `FK_fiscaldata_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.licences
CREATE TABLE IF NOT EXISTS `licences` (
  `id` int(11) DEFAULT NULL,
  `key` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.order
CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `budgetitemid` int(11) NOT NULL,
  `budgetid` int(11) NOT NULL,
  `supplierid` int(11) NOT NULL,
  `total` decimal(10,2) NOT NULL DEFAULT '0.00',
  `statusid` int(11) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `initdate` int(11) DEFAULT NULL COMMENT 'fecha de inicio de la orden de compra',
  `supplydayleft` tinyint(4) NOT NULL DEFAULT '5' COMMENT 'dias restantes para surtir la orden de compra',
  `ordertype` varchar(50) NOT NULL COMMENT 'La orden de compra puede ser de tres tipos: 1-INSUMO; 2-MANO DE OBRA 3-MIXTA ES DECIR DE INSUMO Y MANO DE OBRA',
  `address` varchar(500) NOT NULL DEFAULT '' COMMENT 'direccion de entrega por defaul es la de la plaza del proyecto',
  `deliverto` varchar(50) NOT NULL DEFAULT '' COMMENT 'Persona que recibe el material',
  `phone` varchar(20) NOT NULL DEFAULT '' COMMENT 'Telefono de quien recibe',
  `comment` varchar(100) DEFAULT '' COMMENT 'cualquier comentario util',
  `subtotal` decimal(10,2) NOT NULL DEFAULT '0.00',
  `tax` decimal(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  KEY `FK_order_budgetitem` (`budgetitemid`),
  KEY `FK_order_budget` (`budgetid`),
  KEY `FK_order_supplier` (`supplierid`),
  KEY `FK_order_orderstatus` (`statusid`),
  CONSTRAINT `FK_order_budget` FOREIGN KEY (`budgetid`) REFERENCES `budget` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_order_budgetitem` FOREIGN KEY (`budgetitemid`) REFERENCES `budgetitem` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_order_orderstatus` FOREIGN KEY (`statusid`) REFERENCES `orderstatus` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_order_supplier` FOREIGN KEY (`supplierid`) REFERENCES `supplier` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8 COMMENT='graba la relacion de ordenes de compra';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.orderdetail
CREATE TABLE IF NOT EXISTS `orderdetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `qty` decimal(10,1) NOT NULL DEFAULT '1.0',
  `unit` varchar(10) NOT NULL DEFAULT 'PZA',
  `description` varchar(100) NOT NULL,
  `price` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Es el total o precio = cant * precio unitario',
  `unitprice` decimal(10,2) NOT NULL DEFAULT '0.00',
  `total` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Es el importe',
  `tax` decimal(10,2) NOT NULL,
  `zerotax` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `FK_orderdetail_order` (`orderid`),
  CONSTRAINT `FK_orderdetail_order` FOREIGN KEY (`orderid`) REFERENCES `order` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=424 DEFAULT CHARSET=utf8 COMMENT='guarda el detalle de la orden de compra\r\n';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.orderstatus
CREATE TABLE IF NOT EXISTS `orderstatus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='son los estatus en los que puede estar la orden de compra';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.orderstatustracking
CREATE TABLE IF NOT EXISTS `orderstatustracking` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` int(11) NOT NULL,
  `statusid` int(11) NOT NULL,
  `statusname` varchar(50) NOT NULL,
  `userid` int(11) NOT NULL,
  `trackedon` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_orderstatustracking_orderstatus` (`statusid`),
  KEY `FK_orderstatustracking_order` (`orderid`),
  KEY `FK_orderstatustracking_user` (`userid`),
  CONSTRAINT `FK_orderstatustracking_order` FOREIGN KEY (`orderid`) REFERENCES `order` (`id`) ON DELETE NO ACTION,
  CONSTRAINT `FK_orderstatustracking_orderstatus` FOREIGN KEY (`statusid`) REFERENCES `orderstatus` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_orderstatustracking_user` FOREIGN KEY (`userid`) REFERENCES `user` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='rastreo de los status de la orden de compra, para ver por cuales status va pasando la orden y que usuario es el que asigna dicho estatus.';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.project
CREATE TABLE IF NOT EXISTS `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL COMMENT 'clave',
  `name` varchar(255) NOT NULL COMMENT 'nombre',
  `address` varchar(255) NOT NULL COMMENT 'direccion',
  `location` varchar(50) NOT NULL COMMENT 'plaza',
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.supplier
CREATE TABLE IF NOT EXISTS `supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `name` varchar(255) NOT NULL,
  `rfc` varchar(50) NOT NULL,
  `address` varchar(255) NOT NULL,
  `patronal_record` varchar(50) NOT NULL COMMENT 'registro patronal',
  `agent` varchar(255) DEFAULT NULL COMMENT 'nombre del representante',
  `phone` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.template
CREATE TABLE IF NOT EXISTS `template` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='relacion de plantillas';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.templateitem
CREATE TABLE IF NOT EXISTS `templateitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `templateid` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_templateitem_template` (`templateid`),
  CONSTRAINT `FK_templateitem_template` FOREIGN KEY (`templateid`) REFERENCES `template` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='relacion de partidas de la plantilla';

-- La exportación de datos fue deseleccionada.
-- Volcando estructura para tabla sidic.user
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'apellidos',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'calle y numero',
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rfc` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_number` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fiscal_data` smallint(50) NOT NULL DEFAULT '0',
  `email` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `profile` varchar(10) COLLATE utf8_unicode_ci DEFAULT 'admin',
  `license_id` smallint(6) DEFAULT NULL,
  `activekey` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated` int(11) NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- La exportación de datos fue deseleccionada.
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
