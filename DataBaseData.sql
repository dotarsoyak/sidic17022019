-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.21 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Volcando datos para la tabla sidic.authassignment: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `authassignment` DISABLE KEYS */;
/*!40000 ALTER TABLE `authassignment` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.authitem: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `authitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `authitem` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.authitemchild: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `authitemchild` DISABLE KEYS */;
/*!40000 ALTER TABLE `authitemchild` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.budget: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `budget` DISABLE KEYS */;
INSERT INTO `budget` (`id`, `projectid`, `created`, `update`, `name`, `active`) VALUES
  (1, 1, '2018-03-12 13:18:10', '2018-06-09 16:04:05', 'LOTE 42', 1),
  (2, 2, '2018-03-21 07:58:25', '2018-03-21 07:58:25', 'CONSTRUCCIÓN DE VIVIENDA 252 M2', 1),
  (3, 2, '2018-03-21 07:59:31', '2018-03-21 07:59:31', 'AMPLIACIÓN DE BAÑO PRINCIPAL 230SJV', 1),
  (4, 2, '2018-03-21 08:00:23', '2018-03-21 08:00:23', 'CONSTRUCCION DE TERRAZA POSTERIOR 230SJV', 1),
  (5, 2, '2018-03-21 08:01:12', '2018-03-21 08:01:12', 'FIRME TERRAZA POSTERIOR 230SJV', 1),
  (6, 2, '2018-03-21 08:02:18', '2018-03-21 08:02:18', 'BARDA PATIO POSTERIOR 230SJV', 1),
  (7, 1, '2018-03-21 08:20:49', '2018-03-21 08:20:49', 'CONSTRUCCIÓN DE VIVIENDA 220M2', 1),
  (8, 8, '2018-03-22 09:18:10', '2018-03-22 09:18:10', 'PRESUPUESTO DEMO', 1),
  (9, 9, '2018-06-05 15:11:43', '2018-06-05 15:11:43', 'ESCALERAS', 1);
/*!40000 ALTER TABLE `budget` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.budgetitem: ~51 rows (aproximadamente)
/*!40000 ALTER TABLE `budgetitem` DISABLE KEYS */;
INSERT INTO `budgetitem` (`id`, `budgetid`, `fatherid`, `name`, `budgettop`, `sort`, `status`) VALUES
  (10, 1, 0, 'asdfadsf', 0, 0, 0),
  (11, 1, 0, 'PRELIMNARES', 0, 0, 1),
  (12, 1, 0, 'CIMENTACIONES', 0, 0, 1),
  (13, 1, 0, 'PLOMERIA', 0, 0, 1),
  (14, 1, 13, 'INSTALACION SANITARIA', 0, 0, 1),
  (15, 1, 13, 'INSTALACION PLUVIAL', 0, 0, 1),
  (16, 1, 13, 'INSTALACION DE GAS', 0, 0, 1),
  (17, 1, 0, 'ACABADOS', 0, 0, 1),
  (18, 1, 17, 'PISOS INTERIORES', 0, 0, 1),
  (19, 1, 17, 'PISOS EXTERIORES', 0, 0, 1),
  (20, 1, 17, 'PINTURA', 0, 0, 1),
  (21, 1, 17, 'PIEDRAS EN MUROS Y FACHADAS', 0, 0, 1),
  (22, 1, 0, 'INSTALACION ELECTRICA', 0, 0, 1),
  (23, 1, 0, 'ALBAÑILERIA', 0, 0, 1),
  (24, 1, 23, 'MUROS PLANTA ALTA', 0, 0, 1),
  (25, 1, 23, 'MUROS PLANTA BAJA', 0, 0, 1),
  (26, 1, 23, 'ENJARRES EXTERIORES', 0, 0, 1),
  (27, 1, 23, 'ENJARRES INTERIORES', 0, 0, 1),
  (28, 1, 23, 'ENTORTADOS', 0, 0, 1),
  (29, 1, 10, 'ESTRUCTURA', 0, 0, 1),
  (30, 1, 0, 'ESTRUCTURA', 0, 0, 1),
  (31, 1, 30, 'ENTREPISO', 0, 0, 1),
  (32, 1, 30, 'VIGAS Y COLUMNAS', 0, 0, 1),
  (33, 1, 30, 'AZOTEA', 0, 0, 0),
  (34, 8, 0, 'asdf', 0, 0, 0),
  (35, 2, 0, 'PRELIMINARES', 0, 0, 1),
  (36, 2, 0, 'CIMENTACIONES', 0, 0, 1),
  (37, 2, 0, 'ALBAÑILERIA PLANTA BAJA', 0, 0, 1),
  (38, 2, 0, 'ALBAÑILERIA PLANTA ALTA', 0, 0, 1),
  (39, 2, 0, 'ESTRUCTURA', 0, 0, 1),
  (40, 2, 39, 'ENTREPISOS Y AZOTEAS', 0, 0, 1),
  (41, 2, 39, 'COLUMNAS', 0, 0, 0),
  (42, 2, 39, 'VIGAS Y COLUMNAS', 0, 0, 0),
  (43, 2, 0, 'ALBAÑILERIA AZOTEA', 0, 0, 1),
  (44, 2, 0, 'INSTALACION ELECTRICA', 0, 0, 1),
  (45, 2, 0, 'INSTALACION HIDROSANITARIA Y DE GAS', 0, 0, 1),
  (46, 2, 45, 'INSTALACION HIDRAHULICA', 0, 0, 1),
  (47, 2, 45, 'INSTALACION SANITARIA', 0, 0, 1),
  (48, 2, 45, 'INSTALACION DE GAS', 0, 0, 1),
  (49, 2, 45, 'MUEBLES DE BAÑO LLAVES Y ACCESORIOS', 0, 0, 1),
  (50, 2, 0, 'CARPINTERIA', 0, 0, 1),
  (51, 2, 0, 'CANCELERIA DE ALUMINIO Y CRISTAL', 0, 0, 1),
  (52, 2, 0, 'OBRA EXTERIOR', 0, 0, 1),
  (53, 2, 0, 'ACABADOS', 0, 0, 1),
  (54, 2, 53, 'PISOS Y AZULEJOS', 0, 0, 1),
  (55, 2, 53, 'PIEDRAS Y LOSETAS EN FACHADAS', 0, 0, 1),
  (56, 2, 53, 'PINTURA', 0, 0, 1),
  (57, 2, 39, 'VIGAS Y COLUMNAS', 0, 0, 1),
  (58, 5, 0, 'ALBAÑILERIA', 0, 0, 1),
  (59, 9, 0, 'ALBAÑILERIA', 0, 0, 1),
  (60, 9, 0, 'HERRERIA', 0, 0, 1),
  (61, 7, 0, 'nueva', 0, 0, 1);
/*!40000 ALTER TABLE `budgetitem` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.fiscal: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `fiscal` DISABLE KEYS */;
INSERT INTO `fiscal` (`id`, `userid`, `name`, `lastname`, `address`, `country`, `state`, `city`, `rfc`, `phone_number`, `email`, `created`, `updated`) VALUES
  (17, 17, 'ASDF', NULL, 'SADF', 'SADF', 'SADF', 'ASDF', 'ASDF', 'ASDF', 'ASDF@GMAIL.COM', '2018-02-11 19:48:51', '2018-02-11 19:48:51'),
  (18, 2, 'LUDOVICO VIDAÑA', NULL, 'conocido', 'MEXICO', 'SINALOA', 'CULIACAN', 'tuau800121kn6', '6673583609', 'mileniowebs@gmail.com', '2018-06-08 19:08:43', '2018-06-08 19:08:43');
/*!40000 ALTER TABLE `fiscal` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.licences: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `licences` DISABLE KEYS */;
/*!40000 ALTER TABLE `licences` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.order: ~26 rows (aproximadamente)
/*!40000 ALTER TABLE `order` DISABLE KEYS */;
INSERT INTO `order` (`id`, `budgetitemid`, `budgetid`, `supplierid`, `total`, `statusid`, `created`, `updated`, `initdate`, `supplydayleft`, `ordertype`, `address`, `deliverto`, `phone`, `comment`, `subtotal`, `tax`) VALUES
  (6, 35, 2, 11, 7471.60, 1, '2018-01-21 00:00:00', '2018-06-10 15:04:26', 1414908000, 5, 'MANO DE OBRA', 'DEMO', 'ULISES TRUJILLO A.', '6673', 'ENTREGAR PASADAS LAS 3 PM', 6510.00, 961.60),
  (7, 35, 2, 1, 1464.69, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1559624400, 5, 'MANO DE OBRA', 'CULIACAN', 'ULISES TRUJILLO', '1234', '', 1262.66, 202.03),
  (9, 35, 2, 1, 67860.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1559624400, 5, 'MANO DE OBRA', 'CULIACAN', 'OBED PUERTA', '123', 'J', 58500.00, 9360.00),
  (10, 11, 1, 1, 2700.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1524373200, 5, 'INSUMO', 'DEMO', 'OBED', '6673585539', 'SS', 2327.59, 372.41),
  (11, 14, 1, 1, 1450.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1570165200, 5, 'MANO DE OBRA', 'DEMO', 'OBED PUERTA', '6673585539', '', 1250.00, 200.00),
  (12, 46, 2, 1, 2204.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1578117600, 5, 'MANO DE OBRA', 'CULIACAN', 'HHH', '6666', '', 1900.00, 304.00),
  (13, 11, 1, 1, 116.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1525237200, 5, 'INSUMO', 'DEMO', 'ULISES TRUJILLO A.', 'asdfasdf', '', 100.00, 16.00),
  (15, 48, 2, 1, 2876.80, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1520229600, 5, 'INSUMO', 'SAN JAVIER LOTE 230, DESARROLLO URBANO LA PRIMAVERA', 'JERARDO PUERTA MEDINA', '123', 'ABC', 2480.00, 396.80),
  (16, 58, 5, 6, 7105.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1527051600, 5, 'INSUMOS Y MANO DE OBRA', 'SAN JAVIER LOTE 230, DESARROLLO URBANO LA PRIMAVERA', 'JERARDO PUERTA MEDINA', '6671510773', '', 6125.00, 980.00),
  (17, 59, 9, 14, 1440.00, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1529989200, 5, 'INSUMO', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'OBED PUERTA MEDINA', '6673585539', 'HABLAR ANTES DE ENVIAR.', 0.00, 0.00),
  (18, 59, 9, 12, 1938.06, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1525582800, 5, 'INSUMO', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'OBED PUERTA MEDINA', '6673585539', 'LLAMAR ANTES DE ENTREGAR.', 1670.74, 267.32),
  (19, 60, 9, 6, 701.80, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1529902800, 5, 'INSUMOS Y MANO DE OBRA', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'OBED PUERTA MEDINA', '6673585539', '', 605.00, 96.80),
  (20, 11, 1, 1, 13.92, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1528174800, 5, 'INSUMO', 'DEMO', 'ULISES TRUJILLO A.', '1234234', '', 12.00, 1.92),
  (21, 11, 1, 7, 154.28, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1528261200, 5, 'INSUMO', 'DEMO', 'ULISES TRUJILLO A.', '465465465', '', 133.00, 21.28),
  (24, 59, 9, 12, 1365.99, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1529643600, 5, 'INSUMO', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'OBED PUERTA MEDINA', '6673585539', '', 1177.58, 188.41),
  (25, 59, 9, 12, 515.60, 1, '2018-01-21 00:00:00', '2018-06-06 23:12:56', 1528261200, 5, 'INSUMO', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'JERARDO PUERTA MEDINA', '6673585539', '', 444.48, 71.12),
  (27, 11, 1, 1, 125.00, 1, '2018-06-06 23:22:25', '2018-06-06 23:22:25', 1528261200, 5, 'INSUMO', 'DEMO', 'ULISES TRUJILLO A.', '5454545', '', 0.00, 0.00),
  (28, 61, 7, 1, 520.00, 1, '2018-06-06 23:23:20', '2018-06-06 23:23:20', 1528261200, 5, 'INSUMO', 'DEMO', 'ULISES TRUJILLO A.', '454545', '', 0.00, 0.00),
  (29, 11, 1, 1, 236.00, 1, '2018-06-09 22:10:02', '2018-06-09 22:10:02', 1528520400, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '545646456', '', 100.00, 16.00),
  (30, 11, 1, 1, 239.20, 1, '2018-06-09 22:38:02', '2018-06-09 22:38:02', 1528520400, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '546456456', '', 120.00, 19.20),
  (31, 11, 1, 1, 145.00, 1, '2018-06-10 00:33:08', '2018-06-10 00:33:08', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '4564546546', '', 125.00, 20.00),
  (34, 11, 1, 1, 580.00, 1, '2018-06-10 00:37:38', '2018-06-10 00:37:38', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '234324', '', 500.00, 80.00),
  (35, 11, 1, 1, 348.00, 1, '2018-06-10 00:38:31', '2018-06-10 00:38:31', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '1231234', '', 300.00, 48.00),
  (41, 11, 1, 1, 232.00, 1, '2018-06-10 00:48:07', '2018-06-10 00:48:07', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '1234', '', 200.00, 32.00),
  (43, 11, 1, 1, 232.00, 1, '2018-06-10 00:53:00', '2018-06-10 00:53:00', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', '1234', '', 200.00, 32.00),
  (44, 11, 1, 1, 278.40, 2, '2018-06-10 00:55:41', '2018-06-10 00:55:41', 1528606800, 5, 'INSUMO', 'AVENIDA LOS SAUCES 956, COLONIA LA CAMPIñA', 'ULISES TRUJILLO A.', 'asdfasdf', '', 240.00, 38.40),
  (45, 35, 2, 1, 684.40, 1, '2018-06-10 15:07:33', '2018-06-10 15:07:33', 1538802000, 5, 'INSUMO', 'SAN JAVIER LOTE 230, DESARROLLO URBANO LA PRIMAVERA', 'ULISES TRUJILLO A.', '564564', '', 590.00, 94.40),
  (46, 35, 2, 1, 116.00, 1, '2018-06-10 16:27:51', '2018-06-10 16:27:51', 1521007200, 5, 'INSUMO', 'SAN JAVIER LOTE 230, DESARROLLO URBANO LA PRIMAVERA', 'ULISES TRUJILLO A.', '454654', '', 100.00, 16.00);
/*!40000 ALTER TABLE `order` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.orderdetail: ~5 rows (aproximadamente)
/*!40000 ALTER TABLE `orderdetail` DISABLE KEYS */;
INSERT INTO `orderdetail` (`id`, `orderid`, `qty`, `unit`, `description`, `price`, `unitprice`, `total`, `tax`, `zerotax`) VALUES
  (181, 10, 10.0, 'SAC', 'CEMENTO GRIS 50 KG', 182.00, 0.00, 1820.00, 0.00, 0),
  (182, 10, 10.0, 'PZA', 'VARILLA 3/8"', 88.00, 0.00, 880.00, 0.00, 0),
  (185, 11, 1.0, 'SUB', 'INSTALACION HIDROSANITARIA Y DE GAS. CIMENTACION, ', 1450.00, 0.00, 1450.00, 0.00, 0),
  (191, 13, 1.0, 'PZA', 'ASDFADF1', 100.00, 0.00, 100.00, 0.00, 0),
  (193, 15, 1.0, 'sub', 'SUBCONTRATO PARA INSTALACION DE GAS EN VIVIENDA.  ', 2480.00, 0.00, 2480.00, 0.00, 0),
  (204, 9, 10.0, 'PZA', 'CEMENTO GRIS 50 KG', 1850.00, 0.00, 18500.00, 0.00, 0),
  (205, 9, 20.0, 'PZA', 'VARILLA 3/8"', 2000.00, 0.00, 40000.00, 0.00, 0),
  (206, 12, 1.0, 'SUB', 'MO DE OBRA PARA INSTALACION HIDRAHULICA.', 1900.00, 0.00, 1900.00, 0.00, 0),
  (210, 16, 10.0, 'M2', 'MURO DE BLOCK 15 CMS, INCL. MAT. Y MO.', 250.00, 0.00, 2600.00, 0.00, 0),
  (211, 16, 30.0, 'PZA', 'ANCLAJE DE VAR. DE 3/8" DE 50 CMS', 10.00, 0.00, 300.00, 0.00, 0),
  (212, 16, 60.0, 'PZA', 'COLADO DE CELDAS DE BLOCK DE 15 CMS.', 10.00, 0.00, 600.00, 0.00, 0),
  (213, 16, 21.0, 'M2', 'ENJARRE A BASE DE ARENA Y MORTERO, INCL. MAT . Y M', 125.00, 0.00, 2625.00, 0.00, 0),
  (231, 19, 11.0, 'PZA', 'PLACAS DE HERRERIA PARA ANCLAJE DE ESCALERA.', 55.00, 0.00, 605.00, 0.00, 0),
  (234, 20, 1.0, 'PZA', 'ASDFASDF', 12.00, 0.00, 12.00, 0.00, 0),
  (269, 21, 1.0, 'PZA', 'ASDF', 133.00, 0.00, 133.00, 0.00, 0),
  (271, 17, 1.0, 'PZA', 'MADERA PARA PERALTES VARIAS MEDIDAS.', 1440.00, 0.00, 1440.00, 0.00, 1),
  (278, 18, 2.0, 'PZA', 'CEMENTO GRIS 50 KG', 168.11, 0.00, 336.22, 0.00, 0),
  (279, 18, 12.0, 'PZA', 'VARILLA 3/8"', 111.21, 0.00, 1334.52, 0.00, 0),
  (304, 25, 4.0, 'PZA', 'VARILLA DE 3/8"X12 MTS.', 111.12, 0.00, 444.48, 0.00, 0),
  (305, 24, 3.0, 'KG', 'CLAVO STD 4"', 25.86, 0.00, 77.58, 0.00, 0),
  (306, 24, 10.0, 'KG', 'ALAMBRE RECOCIDO C16', 24.14, 0.00, 241.40, 0.00, 0),
  (307, 24, 10.0, 'KG', 'CLAVO STD 2 1/2"', 25.86, 0.00, 258.60, 0.00, 0),
  (308, 24, 1.0, 'M3', 'GRAVON', 600.00, 0.00, 600.00, 0.00, 0),
  (313, 27, 1.0, 'PZA', 'JñKLJñLJLKJ', 125.00, 0.00, 125.00, 0.00, 1),
  (314, 28, 1.0, 'PZA', 'KJLKJLKJ', 520.00, 0.00, 520.00, 0.00, 1),
  (348, 29, 1.0, 'PZA', 'CEMENTO', 100.00, 0.00, 100.00, 16.00, 0),
  (349, 29, 1.0, 'PZA', 'VARILLA', 120.00, 0.00, 120.00, 0.00, 1),
  (354, 30, 1.0, 'PZA', 'CEMENTO', 100.00, 0.00, 100.00, 0.00, 1),
  (355, 30, 1.0, 'PZA', 'VARILLA', 120.00, 0.00, 120.00, 19.20, 0),
  (369, 31, 1.0, 'PZA', 'CEMENTO', 125.00, 0.00, 125.00, 20.00, 0),
  (372, 34, 5.0, 'PZA', 'CEMENTO', 100.00, 0.00, 500.00, 80.00, 0),
  (373, 35, 3.0, 'PZA', 'CEMENTO', 100.00, 0.00, 300.00, 48.00, 0),
  (379, 41, 2.0, 'PZA', 'CEMENTO', 100.00, 0.00, 200.00, 32.00, 0),
  (381, 43, 2.0, 'PZA', 'CEMENTO', 100.00, 0.00, 200.00, 32.00, 0),
  (385, 44, 2.0, 'PZA', 'CEMENTO', 120.00, 0.00, 240.00, 38.40, 0),
  (406, 7, 10.5, 'PZA', 'ASDF', 120.00, 0.00, 1262.66, 202.03, 0),
  (410, 6, 1.0, 'PZA', 'VARILLA', 150.00, 0.00, 150.00, 24.00, 0),
  (411, 6, 5.0, 'COSTAL', 'CEMENTO', 100.00, 0.00, 500.00, 0.00, 1),
  (412, 6, 1.0, 'SERV', 'TRATAMIENTO ANTI TERMITA', 5860.00, 0.00, 5860.00, 937.60, 0),
  (416, 45, 2.0, 'PZA', 'CEMENTO', 100.00, 0.00, 200.00, 32.00, 0),
  (417, 45, 3.0, 'PZA', 'VARILLA', 50.00, 0.00, 150.00, 24.00, 0),
  (418, 45, 10.0, 'PZA', 'CAL', 24.00, 0.00, 240.00, 38.40, 0),
  (423, 46, 1.0, 'PZA', 'CEMENTO', 100.00, 0.00, 100.00, 16.00, 0);
/*!40000 ALTER TABLE `orderdetail` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.orderstatus: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `orderstatus` DISABLE KEYS */;
INSERT INTO `orderstatus` (`id`, `name`, `active`, `created`) VALUES
  (1, 'COLOCADA', 1, '2018-03-04 10:11:06'),
  (2, 'AUTORIZADA', 1, '2018-03-04 10:11:33'),
  (3, 'CANCELADA', 1, '2018-03-04 10:18:42'),
  (4, 'SURTIDA', 1, '2018-03-19 18:34:44');
/*!40000 ALTER TABLE `orderstatus` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.orderstatustracking: ~16 rows (aproximadamente)
/*!40000 ALTER TABLE `orderstatustracking` DISABLE KEYS */;
INSERT INTO `orderstatustracking` (`id`, `orderid`, `statusid`, `statusname`, `userid`, `trackedon`) VALUES
  (1, 6, 1, 'COLOCADA', 2, '2018-06-09 23:37:05'),
  (2, 6, 2, 'AUTORIZADA', 18, '2018-06-09 23:42:52'),
  (3, 41, 1, 'COLOCADA', 2, '2018-06-10 00:48:07'),
  (5, 43, 1, 'COLOCADA', 2, '2018-06-10 00:53:00'),
  (6, 44, 1, 'COLOCADA', 2, '2018-06-10 00:55:41'),
  (7, 44, 1, 'COLOCADA', 2, '2018-06-10 01:01:41'),
  (8, 44, 2, 'AUTORIZADA', 2, '2018-06-10 01:02:01'),
  (9, 6, 2, 'AUTORIZADA', 2, '2018-06-10 01:02:46'),
  (10, 6, 3, 'CANCELADA', 2, '2018-06-10 01:03:13'),
  (11, 6, 2, 'AUTORIZADA', 2, '2018-06-10 01:07:53'),
  (12, 6, 4, 'SURTIDA', 2, '2018-06-10 01:08:08'),
  (13, 6, 3, 'CANCELADA', 2, '2018-06-10 01:08:27'),
  (14, 7, 2, 'AUTORIZADA', 2, '2018-06-10 11:52:52'),
  (15, 7, 2, 'AUTORIZADA', 2, '2018-06-10 11:53:02'),
  (16, 7, 1, 'COLOCADA', 2, '2018-06-10 12:03:03'),
  (17, 7, 1, 'COLOCADA', 2, '2018-06-10 12:09:54'),
  (18, 7, 1, 'COLOCADA', 2, '2018-06-10 14:53:19'),
  (19, 7, 1, 'COLOCADA', 2, '2018-06-10 14:54:46'),
  (20, 6, 1, 'COLOCADA', 2, '2018-06-10 15:04:32'),
  (21, 6, 1, 'COLOCADA', 2, '2018-06-10 15:04:43'),
  (22, 45, 1, 'COLOCADA', 2, '2018-06-10 15:07:33'),
  (23, 45, 1, 'COLOCADA', 2, '2018-06-10 16:26:57'),
  (24, 46, 1, 'COLOCADA', 2, '2018-06-10 16:27:51'),
  (25, 46, 1, 'COLOCADA', 2, '2018-06-10 16:46:57'),
  (26, 46, 1, 'COLOCADA', 2, '2018-06-10 16:49:01'),
  (27, 46, 1, 'COLOCADA', 2, '2018-06-10 16:49:45'),
  (28, 46, 1, 'COLOCADA', 2, '2018-06-10 16:50:05');
/*!40000 ALTER TABLE `orderstatustracking` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.project: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` (`id`, `code`, `name`, `address`, `location`, `created`) VALUES
  (1, 'demo', 'demo', 'Avenida los Sauces 956, Colonia la Campiña', 'demo', '2018-02-18 21:59:28'),
  (2, '230SJV', 'SAN JAVIER 230', 'SAN JAVIER LOTE 230, DESARROLLO URBANO LA PRIMAVERA', 'Culiacán, Sinaloa', '2018-02-18 21:59:28'),
  (3, '117SJV', 'SAN JAVIER 117', 'SAN JAVIER LOTE 117, DESARROLLO URBANO LA PRIMAVERA', 'CULIACAN', '2018-02-18 21:59:28'),
  (4, '159SAN', 'SAN ANSELMO 159', 'SAN ANSELMO LOTE 159, DESARROLLO URBANO LA PRIMAVERA', 'CULIACAN', '2018-02-18 21:59:28'),
  (5, '1', 'asdfasf', 'asdfasdf', 'asdfdasf', '2018-02-18 21:59:28'),
  (6, '231SJV', '231 SAN JAVIER', 'SAN JAVIER LOTE 231, DESARROLLO URBANO LA PRIMAVERA', 'CULIACAN', '2018-02-19 13:24:58'),
  (7, 'DEPTB', 'DEPARTAMENTOS TB', 'AV. GRAL. ALVARO OBREGON, No. 2481 NTE. COL. TIERRA BLANCA, CULIACAN, SIN, MEX.', 'CULIACAN', '2018-02-19 13:42:03'),
  (8, 'HSSAN', 'LLANTERA HUSA, SUC. SANALONA', 'BLVD. FRANCISCO I. MADERO No. 2468, COL. EL BARRIO, CULIACAN, SIN. MEX.', 'CULIACAN', '2018-02-19 13:46:05'),
  (9, 'IASD-CEN', 'IASD-CENTRAL ', 'C. IGNACIO RAMIREZ #236 OTE, COL. ALMADA, CULIACAN, SINALOA, MEX.', 'CULIACAN', '2018-06-05 15:10:42');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.supplier: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `supplier` DISABLE KEYS */;
INSERT INTO `supplier` (`id`, `code`, `name`, `rfc`, `address`, `patronal_record`, `agent`, `phone`, `email`, `created`, `updated`, `active`) VALUES
  (1, 'asdf ulises', 'asdf', 'asdf', 'asdf', 'TUAU800121kn6', 'asdf', '12341324', 'correo@gmail.com', '2018-06-09 15:11:31', 1519016957, 1),
  (4, 'prov 1', 'mi nombre', 'el rfc', 'conocido', '', '', '', '', '2018-02-18 21:56:19', 0, 1),
  (5, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', '', '', '2018-02-18 21:56:19', 0, 1),
  (6, 'CS01', 'JESUS ROBERTO SOLORZANO SANDOVAL', 'SOSJ671119PX0', 'Miguel Hidalgo Ote. No 696, Col. Centro, Culiacán, Sinaloa, México.', '', 'JESUS ROBERTO SOLORZANO SANDOVAL', '6671000178, 667121309', 'casasolorzano.facturas@gmail.com', '2018-02-18 21:56:19', 0, 1),
  (7, 'KR01', 'MARIA ESTELA KURODA SAN (SUC. CASA KURODA)', 'KUSE5000928TY5', 'Av. Alvaro Obregon, 302 Sur, Col. Centro, Culiacán, Sinaloa, México.', '', 'MARIA ESTELA KURODA SAN', '', '', '2018-02-18 21:56:19', 0, 1),
  (10, 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf@gmail.com', '2018-02-18 21:56:19', 0, 1),
  (11, 'KR02', 'MARIA ESTELA KURODA SAN (SUC. LA CONQUISTA)', 'KUSE500928TY5', 'BLVD. LOLA BELTRAN 4410 PTE CULIACAN, SIN. C.P. 80058', 'TUAU800121KN6', 'MARY QUINTERO', '7232715, CEL. 6673030111', 'mquintero@kuroda.com', '2018-06-09 15:11:54', 1519072359, 1),
  (12, 'GAR01', 'CEMENTOS Y MORTEROS GARSA, S.A. DE C.V.', 'CMG101012NH7', 'BLVD. BENJAMIN HILL #2228 PEMEX C.P. 80180, CULIACAN, SIN. MEX.', '', 'ROSSY  667 169 40 36', '667 275 3383, 667 455 98 29', 'rossy@gmail.com', '2018-06-06 16:29:53', 1528324192, 1),
  (13, 'HDR01', 'HIDRAULICA DE CULIACAN S.A. DE C.V.', 'HCU090819QP9', 'BLVD. FRANCISCO I. MADERO # 2145, COL. MIGUEL HIDALGO, CULIACAN, SIN. MEX. C.P. 80090', '', 'JORGE GASTELUM', '667 336 95 60', 'hidraulicadeculiacan@gmail.com', '2018-06-06 16:34:49', 1528324489, 1),
  (14, 'ARB01', 'Juan Francisco Javier Silvestre Garcia Aguilar (EL ARBOLITO)', 'GAAJ510318362', 'C. MEXICO 68 POSTE 61, COL. BUENOS AIRES, CULIACAN, SIN. MEX.', '', '', '667 717 47 76', 'arbolitocfdi@gmail.com', '2018-06-06 16:38:51', 1528324731, 1),
  (15, 'HOM01', 'HOME DEPOT MEXICO S. DE R.L. DE C.V.', 'HDM001017AS1', 'BLVD. EMILIANO ZAPATA # 2546 PTE., COL. VALLADO NUEVO C.P 80110 CULIACAN, SIN. MEX.', '', '', '667 758 80 00', 'callcenter@homedepot.com.mx', '2018-06-06 16:43:07', 1528324987, 1);
/*!40000 ALTER TABLE `supplier` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.template: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `template` DISABLE KEYS */;
/*!40000 ALTER TABLE `template` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.templateitem: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `templateitem` DISABLE KEYS */;
/*!40000 ALTER TABLE `templateitem` ENABLE KEYS */;

-- Volcando datos para la tabla sidic.user: ~13 rows (aproximadamente)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `username`, `password`, `name`, `lastname`, `address`, `country`, `state`, `city`, `rfc`, `phone_number`, `fiscal_data`, `email`, `profile`, `license_id`, `activekey`, `created`, `updated`, `active`) VALUES
  (2, 'admin', '$2a$13$o3BJSQ2YzQ0k9JbnoKygcOkhtVjP/yETDUD81mjqk2VI/dBja4ZNm', 'ulises trujillo a.', '', 'conocido', 'MEXICO', 'SINALOA', 'CULIACAN', 'tuau800121kn6', '667383609', 1, 'mileniowebs@gmail.com', 'admin', 0, 'normal', '2018-02-18 21:58:47', 1528506523, 1),
  (8, 'user123', '$2a$13$HDagz6EkNIEBrF6xC1DDneed.6Bhiyi1GslAoVp/69s8bBgM.1mC2', 'ulises', NULL, 'conocido', NULL, NULL, NULL, NULL, '6673583609', 0, 'mileniowebs@gmail.com', 'normal', 0, 'normal', '2018-02-18 21:58:47', 1519016574, 1),
  (9, 'gretel', '$2a$13$PibNZ29FiD5WB5C0AMc74eoD8OKnGxtwVLpSW9sjjOT2/gHW.6YCO', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'gretel@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 0, 1),
  (12, 'demo', '$2a$13$o3BJSQ2YzQ0k9JbnoKygcOkhtVjP/yETDUD81mjqk2VI/dBja4ZNm', 'Jerardo Puerta Medina', NULL, 'Calle Cocotero No. 5067, Fracc. Villa del Cedro', 'MEXICO', 'SINALOA', 'CULIACAN', 'PUMJ761201IT7', '6671510773', 0, 'contabilidad.jpm1@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1519054257, 1),
  (13, 'ddemo', '$2a$13$G9eBsQ5uP6xEVGtKgbZ0hOTjrbqjgTViC0neRHk4eMR7FFOzxDxlS', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 'jerardomp@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 0, 1),
  (14, 'xitlalitga', '$2a$13$oJ9ZA/UQFeRa.4oyfr9UAuaSKzE21m35m6oWEzRCAiriOxAnF7l0y', 'Xitlalit Jacquelinne García Arredondo', NULL, 'Calle Cocotero No. 5067, Villa del Cedro, Culiacán, Sinaloa, México.', NULL, NULL, NULL, NULL, '6672350865', 0, 'arq.garcia.xj@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1519072073, 1),
  (15, 'luiseduardo', '$2a$13$iTy4ZcdtyIFMcHLbEBWFyu85DwH56aPyLTpPDsnXgnbQpabpGZqjK', 'Luis Eduardo Padilla Linares', NULL, 'Conocido', NULL, NULL, NULL, NULL, '6671659856', 0, 'obedep@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1519072180, 0),
  (16, 'user1', '$2a$13$jy8zsNjVlKh.ObtiJAmHb.ONL8Bw7L0oX5lRk4v0iDCnHZq/7cJvG', 'usr1', NULL, 'usr1', NULL, NULL, NULL, NULL, 'usr1', 0, 'usr1@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 0, 1),
  (17, 'nemo', '$2a$13$6acTDLcIJ98jmrC/zfBnMOXdSv3mQkZcecVPLZoiNbZ/rf9s/P2t2', 'asdfasdf', NULL, 'JOSE A. ESPIINOZA 2177, FRACC. PORTAFE', 'MEXICO', 'SINALOA', 'CULIACAN', 'TUAU800121KN6', '6673 58 36 09', 0, 'nemo@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 0, 1),
  (18, 'obedpm', '$2a$13$ObpFxxHlsO0uRl4DCZUMZuhgjWdlSy8mDe0qvjYVv6TS8idCDFEsm', 'Obed Enrique Puerta Medina', NULL, 'Calle Tercera No. 101, Col Las Carpas', NULL, NULL, NULL, NULL, '6673585539', 0, 'obed.ep@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1528232869, 1),
  (19, 'luispl', '$2a$13$3jLf/PAPvH1Am2KZ/KlBj.D7945lofGezas4CjBQJPRl.51AexXMa', 'Luis Eduardo Padilla Linares', NULL, 'Conocido', NULL, NULL, NULL, NULL, '6671659856', 0, 'contabilidad.jpm1@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1519016560, 1),
  (20, '123', '$2a$13$NQmlvTHV/Hwn2q5HAzz4CewuGvfsgzI4dogFpbaZeWtshOQ0IEcoi', '123', NULL, '123', NULL, NULL, NULL, NULL, '123', 0, '123@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 0, 1),
  (21, 'xitlalitga', '$2a$13$Sp6RRSrRljXBo86wi8r9AOb93DUdaJqpbFtxvmrnmt733no6w.65W', 'Xitlalit Jacquelinne García Arredondon', NULL, 'Calle Cocotero No. 5067, Villa del Cedro, Culiacán, Sinaloa, México.', NULL, NULL, NULL, NULL, '6672350865', 0, 'arq.garcia.xj@gmail.com', 'admin', NULL, NULL, '2018-02-18 21:58:47', 1519072153, 0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
